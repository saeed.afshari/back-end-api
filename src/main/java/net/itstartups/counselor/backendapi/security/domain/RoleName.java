package net.itstartups.counselor.backendapi.security.domain;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
